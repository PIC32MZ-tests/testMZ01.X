/*! \file  changeFRCDIV.c
 *
 *  \brief Change OSCCONbits.FRCDIV
 *
 *
 *  \author jjmcd
 *  \date January 19, 2016, 8:16 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! changeFRCDIV - Change OSCCONbits.FRCDIV */

/*! Unlock OSCCON, change FRCDIV, lock OSCCON 
 *
 */
void changeFRCDIV(int newValue)
{
  /* Unlock sequence */
  SYSKEY = 0xAA996655;
  SYSKEY = 0x556699AA;
  /* Change the FRC divider */
  OSCCONbits.FRCDIV = newValue;
  /* Lock sequence */
  SYSKEY = 0x33333333;
}
