/*! \file  testMZ01.c
 *
 *  \brief Blink the LED at three different clock settings
 *
 *
 *  \author jjmcd
 *  \date January 9, 2016, 3:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>

void changeFRCDIV( int );

/*! main - */

/*!
 *
 */
int main(void)
{
  long i;
  int j;
    
  TRISFbits.TRISF2 = 0;     /* LED pin output */
  
  LATFbits.LATF2 = 1;   /* LED on */
  
  /* Blink the LED 10 times at 3 different clock settings 
   * 
   * LATFbits.LATF2 ^= 1 toggles the LED state so doing that
   * 20 times makes LED blink 10 times.
   * 
   * Clock should be 7.37, 1.84 and 0.46 MHz
   * 
   * Loop count of 50,000 makes for a very fast, but visible
   * blink at 7.37 MHz and around a 1 or 2 second blink at 0.46.
   */
  while(1)
    {
      changeFRCDIV(0); /* Divide by 1 */
      for (j = 0; j < 20; j++)
        {
          for (i = 0; i < 50000; i++)
            ;
          LATFbits.LATF2 ^= 1;
        }

      changeFRCDIV(2); /* Divide by 4 */
      for (j = 0; j < 20; j++)
        {
          for (i = 0; i < 50000; i++)
            ;
          LATFbits.LATF2 ^= 1;
        }

      changeFRCDIV(4); /* Divide by 16 */
      for (j = 0; j < 20; j++)
        {
          for (i = 0; i < 50000; i++)
            ;
          LATFbits.LATF2 ^= 1;
        }
    }

  return 0;
}
